# Go net/http pprof example

## Main purposes

-  making a small API using net/http without all that routing things like chi/gorilla etc...
-  adding pprof to it

## Example

- `go tool pprof goprofex http://127.0.0.1:3000/debug/pprof/profile` should work