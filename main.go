package main

import (
	"context"
	"encoding/json"
	"net/http"
	"net/http/pprof"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
)

type apiError struct {
	Message string `json:"message"`
}
type messageResponse struct {
	Message string `json:"message"`
}

func respondWithJSON(w http.ResponseWriter, status int, payload interface{}) error {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	return json.NewEncoder(w).Encode(payload)
}

//nolint:unparam // this ctx wil be used for the logger
func writeError(ctx context.Context, w http.ResponseWriter, status int, message string) {
	if err := respondWithJSON(w, status, apiError{Message: message}); err != nil {
		logEntryFromContext(ctx).WithError(err).Error("write response")
	}
}

//nolint:unparam // this ctx wil be used for the logger
func writeSuccessful(ctx context.Context, w http.ResponseWriter, payload interface{}) {
	if err := respondWithJSON(w, http.StatusOK, payload); err != nil {
		logEntryFromContext(ctx).WithError(err).Error("write response")
	}
}

func getMessageHello(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	log := logEntryFromContext(ctx)

	if r.Method != http.MethodGet {
		writeError(ctx, w, http.StatusMethodNotAllowed, http.StatusText(http.StatusMethodNotAllowed))
	}

	log.Info("hello!")
	writeSuccessful(ctx, w, messageResponse{Message: "hello!"})
}

type loggerCtxKey struct{}

func contextWithLogEntry(ctx context.Context, logEntry *logrus.Entry) context.Context {
	return context.WithValue(ctx, loggerCtxKey{}, logEntry)
}
func logEntryFromContext(ctx context.Context) *logrus.Entry {
	le, ok := ctx.Value(loggerCtxKey{}).(*logrus.Entry)
	if !ok {
		le = logrus.NewEntry(logrus.New())
	}
	return le
}

func newLoggingMiddleware(log *logrus.Entry) func(next http.Handler) http.Handler {
	return func(handler http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			nextEntry := log.WithFields(logrus.Fields{
				"method":     r.Method,
				"path":       r.URL.Path,
				"request_id": uuid.New().String(),
			})

			handler.ServeHTTP(w, r.WithContext(contextWithLogEntry(r.Context(), nextEntry)))
		})
	}
}

func main() {
	log := logrus.New()
	log.SetFormatter(&logrus.JSONFormatter{})
	logEntry := logrus.NewEntry(log)

	r := http.NewServeMux()
	r.HandleFunc("/message/hello", getMessageHello)
	r.HandleFunc("/debug/pprof/", pprof.Index)
	r.HandleFunc("/debug/pprof/cmdline", pprof.Cmdline)
	r.HandleFunc("/debug/pprof/profile", pprof.Profile)
	r.HandleFunc("/debug/pprof/symbol", pprof.Symbol)
	r.HandleFunc("/debug/pprof/trace", pprof.Trace)

	handler := newLoggingMiddleware(logEntry)(r)

	const addr = ":3000"

	srv := &http.Server{
		Addr:    addr,
		Handler: handler,
	}
	defer srv.Close()

	stop := make(chan os.Signal, 1)
	signal.Notify(stop,
		syscall.SIGHUP,
		syscall.SIGINT,
		syscall.SIGTERM,
		syscall.SIGQUIT)

	go func() {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatal("start server: ", err)
		}
	}()
	log.WithField("addr", addr).Info("server started listening")

	<-stop
	log.Info("caught stop signal")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	if err := srv.Shutdown(ctx); err != nil {
		log.WithError(err).Fatal("server shutdown failed")
	}

	log.Info("server stopped properly")
}
